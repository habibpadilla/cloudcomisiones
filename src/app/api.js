import { setAxiosHeader } from './auth'

setAxiosHeader()

function Users (url = 'user') {
    return {
        getOne: (id) => axios.get(`${url}/${id}`),
        getAll: () => axios.get(`${url}`),
        update: (id, toUpdate) => axios.put(`${url}/${id}`, toUpdate),
        create: (toCreate) => axios.post(url, toCreate),
        delete: (id) => axios.delete(`${url}/${id}`),
        updateRole: (id, roleId) => axios.put(`${url}/${id}/roles/${roleId}`),
        authorizeRoles: (id,toUpdate) => axios.put(`${url}/${id}/roles/`,toUpdate),
    }
}

function Roles (url = 'roles') {
    return {
        getOne: (id) => axios.get(`${url}/${id}`),
        getAll: () => axios.get(`${url}`),
        getPaginate: () => axios.get(`${url}?pagination=true`),
        update: (id, toUpdate) => axios.put(`${url}/${id}`, toUpdate),
        create: (toCreate) => axios.post(url, toCreate),
        delete: (id) => axios.delete(`${url}/${id}`)
    }
}
function assistances (url = 'assistances') {
    return {
        getOne: (id) => axios.get(`${url}/${id}`),
        getAll: () => axios.get(`${url}`),
        update: (id, toUpdate) => axios.put(`${url}/${id}`, toUpdate),
        create: (toCreate) => axios.post(url, toCreate),
        delete: (id) => axios.delete(`${url}/${id}`),
        updateRole: (id, roleId) => axios.put(`${url}/${id}/assistances/${assistanceId}`),
        authorizeRoles: (id,toUpdate) => axios.put(`${url}/${id}/assistances/`,toUpdate),
    }
}
function campaigns (url = 'campaigns') {
    return {
        getOne: (id) => axios.get(`${url}/${id}`),
        getAll: () => axios.get(`${url}`),
        update: (id, toUpdate) => axios.put(`${url}/${id}`, toUpdate),
        create: (toCreate) => axios.post(url, toCreate),
        delete: (id) => axios.delete(`${url}/${id}`),
        updateRole: (id, roleId) => axios.put(`${url}/${id}/campaigns/${campaignsId}`),
        authorizeRoles: (id,toUpdate) => axios.put(`${url}/${id}/campaigns/`,toUpdate),
    }
}

function baseCampaign (url = 'base-campaign') {
    return {
        getOne: (id) => axios.get(`${url}/${id}`),
        getAll: () => axios.get(`${url}`),
        update: (id, toUpdate) => axios.put(`${url}/${id}`, toUpdate),
        create: (toCreate) => axios.post(url, toCreate),
        delete: (id) => axios.delete(`${url}/${id}`),
        updateRole: (id, roleId) => axios.put(`${url}/${id}/base-campaign/${campaignsId}`),
        authorizeRoles: (id,toUpdate) => axios.put(`${url}/${id}/base-campaign/`,toUpdate),
    }
}

function gerenteCampaign (url = 'gerente-campaigns') {
    return {
        getOne: (id) => axios.get(`${url}/${id}`),
        getAll: () => axios.get(`${url}`),
        update: (id, toUpdate) => axios.put(`${url}/${id}`, toUpdate),
        create: (toCreate) => axios.post(url, toCreate),
        delete: (id) => axios.delete(`${url}/${id}`),
        updateRole: (id, roleId) => axios.put(`${url}/${id}/gerente-campaigns/${campaignsId}`),
        authorizeRoles: (id,toUpdate) => axios.put(`${url}/${id}/gerente-campaigns/`,toUpdate),
    }
}
function productsCampaign (url = 'products-campaign') {
    return {
        getOne: (id) => axios.get(`${url}/${id}`),
        getAll: () => axios.get(`${url}`),
        update: (id, toUpdate) => axios.put(`${url}/${id}`, toUpdate),
        create: (toCreate) => axios.post(url, toCreate),
        delete: (id) => axios.delete(`${url}/${id}`),
        updateRole: (id, roleId) => axios.put(`${url}/${id}/products-campaign/${campaignsId}`),
        authorizeRoles: (id,toUpdate) => axios.put(`${url}/${id}/products-campaign/`,toUpdate),
    }
}

function floorsCampaign (url = 'floors-campaign') {
    return {
        getOne: (id) => axios.get(`${url}/${id}`),
        getAll: () => axios.get(`${url}`),
        update: (id, toUpdate) => axios.put(`${url}/${id}`, toUpdate),
        create: (toCreate) => axios.post(url, toCreate),
        delete: (id) => axios.delete(`${url}/${id}`),
        updateRole: (id, roleId) => axios.put(`${url}/${id}/floors-campaign/${campaignsId}`),
        authorizeRoles: (id,toUpdate) => axios.put(`${url}/${id}/floors-campaign/`,toUpdate),
    }
}

function baseCampaignDirector (url = 'base-campaign-director') {
    return {
        getOne: (id) => axios.get(`${url}/${id}`),
        getAll: () => axios.get(`${url}`),
        update: (id, toUpdate) => axios.put(`${url}/${id}`, toUpdate),
        create: (toCreate) => axios.post(url, toCreate),
        delete: (id) => axios.delete(`${url}/${id}`),
        updateRole: (id, roleId) => axios.put(`${url}/${id}/base-campaign-director/${campaignsId}`),
        authorizeRoles: (id,toUpdate) => axios.put(`${url}/${id}/base-campaign-director/`,toUpdate),
    }
}

function baseCampaignCoordinador (url = 'base-campaign-coordinador') {
    return {
        getOne: (id) => axios.get(`${url}/${id}`),
        getAll: () => axios.get(`${url}`),
        update: (id, toUpdate) => axios.put(`${url}/${id}`, toUpdate),
        create: (toCreate) => axios.post(url, toCreate),
        delete: (id) => axios.delete(`${url}/${id}`),
        updateRole: (id, roleId) => axios.put(`${url}/${id}/base-campaign-coordinador/${campaignsId}`),
        authorizeRoles: (id,toUpdate) => axios.put(`${url}/${id}/base-campaign-coordinador/`,toUpdate),
    }
}

function baseCampaignUser (url = 'base-campaign-user') {
    return {
        getOne: (id) => axios.get(`${url}/${id}`),
        getAll: () => axios.get(`${url}`),
        update: (id, toUpdate) => axios.put(`${url}/${id}`, toUpdate),
        create: (toCreate) => axios.post(url, toCreate),
        delete: (id) => axios.delete(`${url}/${id}`),
        updateRole: (id, roleId) => axios.put(`${url}/${id}/base-campaign-user/${campaignsId}`),
        authorizeRoles: (id,toUpdate) => axios.put(`${url}/${id}/base-campaign-user/`,toUpdate),
    }
}
function directoresCampaigns (url = 'directores-campaigns') {
    return {
        getOne: (id) => axios.get(`${url}/${id}`),
        getAll: () => axios.get(`${url}`),
        update: (id, toUpdate) => axios.put(`${url}/${id}`, toUpdate),
        create: (toCreate) => axios.post(url, toCreate),
        delete: (id) => axios.delete(`${url}/${id}`),
        updateRole: (id, roleId) => axios.put(`${url}/${id}/directores-campaigns/${campaignsId}`),
        authorizeRoles: (id,toUpdate) => axios.put(`${url}/${id}/directores-campaigns/`,toUpdate),
    }
}
function coordinadorCampaigns (url = 'coordinador-campaigns') {
    return {
        getOne: (id) => axios.get(`${url}/${id}`),
        getAll: () => axios.get(`${url}`),
        update: (id, toUpdate) => axios.put(`${url}/${id}`, toUpdate),
        create: (toCreate) => axios.post(url, toCreate),
        delete: (id) => axios.delete(`${url}/${id}`),
        updateRole: (id, roleId) => axios.put(`${url}/${id}/coordinador-campaigns/${campaignsId}`),
        authorizeRoles: (id,toUpdate) => axios.put(`${url}/${id}/coordinador-campaigns/`,toUpdate),
    }
}


export default {
    Users,
    Roles,
    assistances,
    campaigns,
    baseCampaign,
    gerenteCampaign,
    productsCampaign,
    floorsCampaign,
    baseCampaignDirector,
    baseCampaignCoordinador,
    baseCampaignUser,
    directoresCampaigns,
    coordinadorCampaigns,
}
  
