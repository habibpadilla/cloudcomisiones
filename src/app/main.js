// Polyfills
import 'es6-promise/auto'
import 'babel-polyfill'
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'

import { ColorThemePlugin } from 'vuestic-ui/src/services/ColorThemePlugin'
import store from '../store/index'
import router from '../router/index'
import VuesticPlugin from 'vuestic-ui/src/components/vuestic-plugin'
import '../i18n/index'
import YmapPlugin from 'vue-yandex-maps'
import VueClipboard from 'vue-clipboard2'
import VeeValidate from 'vee-validate'

import './axiosBootstrap.js'
import { isLoggedIn } from './auth'

import '../metrics'

import EventBus from '../../src/components/event-bus'

Vue.use(EventBus)

// NOTE: workaround for VeeValidate + vuetable-2
Vue.use(VeeValidate, { fieldsBagName: 'formFields' })

Vue.use(VuesticPlugin)
Vue.use(YmapPlugin)
Vue.use(VueClipboard)

Vue.use(ColorThemePlugin,
  {
    // Add or change theme colors here
    themes: {
      // primary: '#f06595',
      // blurple: '#7289DA',
    },
  })


router.beforeEach((to, from, next) => {
  store.commit('setLoading', true)

  console.log(isLoggedIn())

  if (!to.matched.some(record => record.meta.isPublic) && !isLoggedIn()) {
    next({
      path: '/auth/login', query: { redirect: to.fullPath },
    })
 
  } else {

    const nearestWithTitle = to.matched.slice().reverse().find(record => record.meta && record.meta.title);

    if (nearestWithTitle){
        document.title = to.meta.title
    }

    next();
  }
  
})

router.afterEach((to, from) => {
  store.commit('setLoading', false)
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
})